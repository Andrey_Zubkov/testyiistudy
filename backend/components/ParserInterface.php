<?php
namespace backend\components;

/**
 * Interface ParserInterface
 * @package backend\components
 */
interface ParserInterface
{
    /**
     * @param string $data
     * @return array
     */
    public function parseData(string $data) : array;
}