<?php


namespace backend\components;

use backend\components\parser\ParserJson;
use backend\components\parser\ParserString;
use backend\components\parser\ParserXml;
use backend\components\parser\ProviderJson;
use backend\components\parser\ProviderString;
use backend\components\parser\ProviderXml;

/**
 * Class ParserFactory
 * @package backend\components
 */
class ParserFactory
{
    /**
     *
     */
    const PROVIDER_TYPE_JSON    = 'json';
    /**
     *
     */
    const PROVIDER_TYPE_STRING  = 'string';
    /**
     *
     */
    const PROVIDER_TYPE_XML     = 'xml';


    /**
     * @param string $provider
     * @return ParserInterface
     */
    public function create(string $provider): ParserInterface
    {
        switch ($provider) {
            case self::PROVIDER_TYPE_XML:
                return new ParserXml();
                break;
            case self::PROVIDER_TYPE_STRING:
                return new ParserString();
                break;
            case self::PROVIDER_TYPE_JSON:
            default:
                return new ParserJson();
        }
    }

}