<?php
namespace backend\components\parser;

use backend\components\ParserInterface;

/**
 * Class ProviderString
 * @package backend\components\parser
 */
class ParserString implements ParserInterface
{
    /**
     * @param string $data
     * @return array
     */
    public function parseData(string $data): array
    {
        $data = json_decode($data);
        $attributes = $this->getAttributesModel($data['data']);
        return $attributes;
    }

    /**
     * @param string $data
     * @return array
     */
    private function getAttributesModel(string $data): array
    {
        //some code for parsing string
        return [];
    }
}