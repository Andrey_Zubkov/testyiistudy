<?php
namespace backend\components\parser;

use backend\components\ParserInterface;

class ParserJson implements ParserInterface
{
    public function parseData(string $data): array
    {
        $data = json_decode($data);
        $attributes = $data['data'];
        return $attributes;
    }
}