<?php

namespace backend\controllers;

use backend\components\FactoryProvider;
use backend\components\ParserFactory;
use backend\models\TestModel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-data'],
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    public function actionGetData()
    {
        if (Yii::$app->request->isPost && !empty( $data = Yii::$app->request->post())) {
           $factory = new ParserFactory();
           $data = json_decode($data);
           $parserType = $data['parser'];
           $parser = $factory->create($parserType);
           $testModel = new TestModel();
           if ($testModel->load($parser->parseData($data)) && $testModel->save()) {
                return true;
           }
        }

        return false;
    }


}